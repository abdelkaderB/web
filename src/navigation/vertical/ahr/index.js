import Employees from "./employees"
import Preferences from "./preferences"
import Tasks from "./tasks"
import Teams from "./teams"
import Notifications from "./notifications"

export default [  
    {
        id:'header',
        header: 'Apps & Pages'
    },
  ...Preferences,
  ...Teams,
  ...Employees,
  ...Tasks,
  ...Notifications
]
