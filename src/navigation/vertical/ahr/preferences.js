// ** Icons Import
import { Settings, Shield, MapPin, Circle } from 'react-feather'

export default [
  {
    id: 'preferences',
    title: 'Preferences',
    icon: <Settings size={20} />,
    children: [
      {
        id: 'sites',
        title: 'Sites',
        icon: <MapPin size={12} />,
        navLink: '/ahr/sites'
      },
      {
        id: 'roles-permissions',
        title: 'Roles & Permissions',
        icon: <Shield size={20} />,
        children: [
        {
            id: 'roles',
            title: 'Roles',
            icon: <Circle size={12} />,
            navLink: '/apps/roles'
        },
        {
            id: 'permissions',
            title: 'Permissions',
            icon: <Circle size={12} />,
            navLink: '/apps/permissions'
        }
        ]
    }
    ]
  }
]
