// ** Icons Import
import { User, Circle } from 'react-feather'

export default [
    {
      id: 'employees',
      title: 'Employees',
      icon: <User size={20} />,
      navLink: '/ahr/employees/list'
    }
]
