// ** Icons Import
import { Bell } from 'react-feather'

export default [
    {
      id: 'notifications',
      title: 'Notifications',
      icon: <Bell size={20} />,
      navLink: '/ahr/notifications'
    }
]
