// ** Icons Import
import { Codesandbox } from 'react-feather'

export default [
    {
        id: 'teams',
        title: 'Teams',
        icon: <Codesandbox size={20} />,
        navLink: '/ahr/teams'
    }
]