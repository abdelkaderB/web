// ** Icons Import
import { CheckSquare } from 'react-feather'

export default [
    {
        id: 'tasks',
        title: 'Tasks',
        icon: <CheckSquare size={20} />,
        navLink: '/ahr/tasks'
    }
]