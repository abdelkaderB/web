import mock from '../mock'

const data = {
  sites: [
    {
      id: 1,
      name: 'Rabat - Headquarter',
      address: {
        city: "Rabat",
        zipCode: 10000,
        addressLine: "28 rue attakaddoum"
      },
      employees: 29,
      gpsCoordinates: [
        { longitude: 15.029582138333208, latitude: 30.006408691406254 },
        { longitude: 15.00040124601316, latitude: 29.959716796875004 },
        { longitude: 15.029582138333208, latitude: 30.006408691406254 }
      ],
      responseTime: 1
    },
    {
      id: 2,
      name: 'Oujda - site',
      address: {
        city: "Oujda",
        zipCode: 60000,
        addressLine: "13 hay attadamoun"
      },
      employees: 16,
      gpsCoordinates: [
        { longitude: 15.029582138333208, latitude: 30.006408691406254 },
        { longitude: 15.00040124601316, latitude: 29.959716796875004 },
        { longitude: 15.029582138333208, latitude: 30.006408691406254 }
      ],
      responseTime: 1
    }
  ]
}

mock.onGet('/sites/all').reply(() => [200, data.sites])

// ------------------------------------------------
// POST: Add new site
// ------------------------------------------------
mock.onPost('/sites').reply(config => {
  const { site } = JSON.parse(config.data)

  const { length } = data.sites
  let lastIndex = 0
  if (length) {
    lastIndex = data.sites[length - 1].id
  }
  site.id = lastIndex + 1

  data.sites.push(site)

  return [201, { site }]
})

// ------------------------------------------------
// PUT: Update site
// ------------------------------------------------
mock.onPut('/sites').reply(config => {
  const { site } = JSON.parse(config.data)
  data.sites = [...data.sites.filter(o => o.id !== site.id), site]
  data.sites.sort((a, b) => a.id - b.id)
  return [201, { site }]
})

// ------------------------------------------------
// PUT: Update site
// ------------------------------------------------
mock.onDelete('/sites').reply(config => {
  const siteId = parseInt(config.siteId)
  data.sites = [...data.sites.filter(o => o.id !== siteId)]
  data.sites.sort((a, b) => a.id - b.id)
  return [201, { siteId }]
})

