import mock from '../mock'

// ** Utils
import { paginateArray } from '../utils'

const data = {
  teams: [
    {
      id: 1,
      name: 'Management',
      supervisor: {
        id: 1,
        name: "Galen Slixby"
      },
      employeesCount:20,
      assignedTo: ['administrator'],
      createdDate: '14 Apr 2021, 8:43 PM',
      employeeCount: 5
    },
    {
      id: 2,
      supervisor: {
        id: 2,
        name:"Halsey Redmore"
      },
      employeesCount:20,
      assignedTo: ['administrator'],
      name: 'Cleaning Services',
      createdDate: '16 Sep 2021, 5:20 PM',
      employeeCount: 33
    },
    {
      id: 3,
      name: 'Security and guard services',
      supervisor: {
        id: 4,
        name: "Cyrill Risby"
      },
      employeesCount:20,
      createdDate: '14 Oct 2021, 10:20 AM',
      assignedTo: ['administrator', 'manager'],
      employeeCount: 23
    },
    {
      id: 4,
      name: 'Gardening and pool cleaning service',
      supervisor: {
        id: 4,
        name: "Cyrill Risby"
      },
      employeesCount:20,
      createdDate: '14 Oct 2021, 10:20 AM',
      assignedTo: ['administrator', 'manager'],
      employeeCount: 7
    }
  ]
}


// GET ALL DATA
mock.onGet('/teams/all-data').reply(200, data.teams)

// ------------------------------------------------
// GET: Return Teams List
// ------------------------------------------------

mock.onGet('/teams/data').reply(config => {
  const { q = '', perPage = 10, page = 1, assignedTo = '' } = config.params
  const queryLowered = q.toLowerCase()
  const filteredData = data.teams.filter(team => {
    if (assignedTo !== '') {
      return (
        (team.name.toLowerCase().includes(queryLowered) ||
        team.createdDate.toLowerCase().includes(queryLowered)) &&
        team.assignedTo.includes(assignedTo)
      )
    } else {
      return (
        team.name.toLowerCase().includes(queryLowered) ||
        team.createdDate.toLowerCase().includes(queryLowered)
      )
    }
  })

  return [
    200,
    {
      params: config.params,
      allData: data.teams,
      total: filteredData.length,
      teams: paginateArray(filteredData, perPage, page)
    }
  ]
})

// POST: Add new team
mock.onPost('/teams/add-team').reply(config => {
  // Get team from post data
  const team = JSON.parse(config.data).team

  const { length } = data.teams
  let lastIndex = 0
  if (length) {
    lastIndex = data.teams[length - 1].id
  }
  team.id = lastIndex + 1
  team.employeeCount = 0

  team.assignedTo = ['administrator']

  const now = new Date()
  const months = now
    .toLocaleDateString('en-US', {
      year: 'numeric',
      month: 'short',
      day: 'numeric'
    })
    .replace(',', '')
  const time = now.toLocaleString('en-US', { hour: 'numeric', minute: 'numeric', hour12: true })

  const monthsArr = months.split(' ')
  /*eslint-disable */
  const moveElement = (array, sourceIndex, destinationIndex) => {
    return array.map(a =>
      a === sourceIndex
        ? array.find(a => a === destinationIndex)
        : a === destinationIndex
        ? array.find(a => a === sourceIndex)
        : a
    )
  }

  const finalDate = moveElement(monthsArr, monthsArr[0], monthsArr[1]).join(' ')

  team.createdDate = `${finalDate}, ${time}`

  data.teams.unshift(team)

  return [201, { team }]
})

mock.onPost('/teams/update-team').reply(config => {
  // Get team from post data
  const { id, name, supervisor } = JSON.parse(config.data)

  data.teams.find(i => {
    if (i.id === id) {
      i.name = name
      i.supervisor = supervisor
    }
  })

  return [201]
})

// DELETE: Deletes Teams
mock.onDelete('/teams/delete').reply(config => {
  // Get  id from URL
  let teamID = config.id

  // Convert Id to number
  teamID = Number(teamID)

  const teamIndex = data.teams.findIndex(t => t.id === teamID)
  data.teams.splice(teamIndex, 1)

  return [200]
})
