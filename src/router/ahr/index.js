// ** React Imports
import { lazy } from 'react'

const AhrRoutes = [
  {
    path: '/ahr/sites',
    exact: true,
    component: lazy(() => import('../../views/ahr/sites'))
  },
  {
    path: '/ahr/employees/list',
    exact: true,
    component: lazy(() => import('../../views/ahr/employees/list'))
  },
  {
    path: '/ahr/employees/view/:id',
    component: lazy(() => import('../../views/ahr/employees/view')),
    meta: {
      navLink: '/ahr/employees/view'
    }
  },
  {
    path: '/ahr/tasks',
    exact: true,
    appLayout: true,
    className: 'todo-application',
    component: lazy(() => import('../../views/ahr/tasks'))
  },
  {
    path: '/ahr/teams',
    component: lazy(() => import('../../views/ahr/teams'))
  },
  {
    path: '/ahr/notifications',
    component: lazy(() => import('../../views/ahr/notifications'))
  }
]

export default AhrRoutes
