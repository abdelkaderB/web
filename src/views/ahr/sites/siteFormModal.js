import { Fragment, useRef, useState, useEffect } from 'react'
import * as L from 'leaflet'
import EsriLeafletGeoSearch from "react-esri-leaflet/plugins/EsriLeafletGeoSearch"
//import * as esri from 'esri-leaflet'
import * as Geocoding from 'esri-leaflet-geocoder'
import { MapContainer, TileLayer, Marker, Popup, useMapEvents, Polyline, LayersControl } from 'react-leaflet'
import { Delete } from 'react-feather'

import {
    Button,
    Row,
    Col,
    Card,
    CardBody,
    Modal,
    Label,
    Input,
    ModalBody,
    InputGroup,
    ModalHeader,
    CardHeader,
    CardTitle
} from 'reactstrap'

// ** FAQ Illustrations
import 'leaflet/dist/leaflet.css'
const Icon1 = new L.Icon({
    iconUrl: require('@src/assets/images/svg/map-marker.png').default,
    iconRetinaUrl: require('@src/assets/images/svg/map-marker.png').default,
    iconAnchor: [15, 30],
    popupAnchor: [10, -44],
    iconSize: [30, 30],
    shadowSize: [68, 95],
    shadowAnchor: [20, 92]
})

const MyPopupMarker = ({ position, content }) => {
    return (
        <Marker position={position} icon={Icon1} interactive={false}>
            <Popup>{content}</Popup>
        </Marker >
    )
}

const MyMarkersList = ({ markers, setMarkers }) => {
    const items = markers.map(({ key, ...props }) => <MyPopupMarker key={key} {...props} />)
    useMapEvents({
        dblclick(e) {
            if (markers.length === 0) {
                setMarkers([
                    { key: 0, position: e.latlng, content: e.latlng },
                    { key: 1, position: e.latlng, content: e.latlng }
                ])
            } else {
                markers.splice(markers.length - 1, 0, { key: markers.length - 1, position: e.latlng, content: e.latlng })
                markers.at(-1).key = markers.length - 1
                setMarkers([...markers])
            }
        }
    })
    return <Fragment>{items}</Fragment>
}

const SiteFormModal = ({ show, editShow, onSubmit, selectedSite }) => {
    const toMarkers = (gpsCoordinates) => {
        gpsCoordinates = gpsCoordinates === undefined ? [] : gpsCoordinates
        return gpsCoordinates.map((it, i) => ({ key: i, position: [it.longitude, it.latitude], content: it.latitude }))
    }
    const [markers, setMarkers] = useState([])

    const nameRef = useRef(null)
    const cityRef = useRef(null)
    const addressLineRef = useRef(null)
    const zipCodeRef = useRef(null)
    const handleSumbit = () => {
        const site = {
            ...selectedSite,
            name: nameRef.current.value,
            address: { addressLine: addressLineRef.current.value, city: cityRef.current.value, zipCode: zipCodeRef.current.value },
            gpsCoordinates: markers.map(marker => ({longitude: marker.position.lat, latitude: marker.position.lng}))
        }
        onSubmit(site)
    }
    useEffect(() => {
        setMarkers(toMarkers(selectedSite.gpsCoordinates))
    }, [selectedSite.gpsCoordinates])

    return (
        <Modal
            isOpen={show}
            toggle={() => editShow(!show)}
            className='modal-dialog-centered'
            onClosed={() => { }}
        >
            <ModalHeader className='bg-transparent' toggle={() => editShow(!show)}></ModalHeader>
            <ModalBody className='px-sm-5 mx-50 pb-5'>
                <h1 className='text-center mb-1'>Add New Site</h1>
                <p className='text-center'>Add new site for your company</p>
                <Row tag='form' className='gy-1 gx-2 mt-75'>
                    <Col xs={12}>
                        <Label className='form-label' for='name'>
                            Site name
                        </Label>
                        <InputGroup>
                            <Input type="text" innerRef={nameRef} defaultValue={selectedSite.name} id='name' name='name' placeholder='Paris - HeadQuarter' />
                        </InputGroup>
                    </Col>
                    <Col md={6}>
                        <Label className='form-label' for='addressLine'>
                            Adress line
                        </Label>
                        <Input type="text" innerRef={addressLineRef} defaultValue={selectedSite.address.addressLine} id='addressLine' placeholder='13 hay attadamoun' />
                    </Col>
                    <Col xs={6} md={3}>
                        <Label className='form-label' for='city'>
                            City
                        </Label>
                        <Input
                            id='city'
                            name='city'
                            placeholder='paris'
                            className='form-control'
                            type="text" innerRef={cityRef}
                            defaultValue={selectedSite.address.city}
                        />
                    </Col>
                    <Col xs={6} md={3}>
                        <Label className='form-label' for='zipCode'>
                            Zip Code
                        </Label>
                        <Input type="text" innerRef={zipCodeRef} defaultValue={selectedSite.address.zipCode} id='zipCode' name='zipCode' placeholder='65412' className='form-control' options={{ blocks: [3] }} />
                    </Col>
                    <Col xs={12}>
                        <Card className='overflow-hidden'>
                            <CardHeader>
                                <CardTitle tag='h4'>Basic</CardTitle>

                                <Button.Ripple className='btn-icon' color='flat-dark'
                                    onClick={() => {
                                        setMarkers([])
                                    }}
                                >
                                    <Delete size={16} />
                                </Button.Ripple>
                            </CardHeader>
                            <CardBody>
                                <MapContainer center={[15.1752, 29.9400]} zoom={10} className='leaflet-map' doubleClickZoom={false}>
                                    <LayersControl position="topleft" collapsed={false}>
                                        <LayersControl.BaseLayer name="Tiled Map Layer" checked={true}>
                                            <TileLayer url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png" attribution="&copy; <a href=&quot;https://www.openstreetmap.org/copyright&quot;>OpenStreetMap</a> contributors" />
                                        </LayersControl.BaseLayer>
                                    </LayersControl>
                                    <MyMarkersList markers={markers} setMarkers={setMarkers} />
                                    <Polyline pathOptions={{ fillColor: 'purple', fill: true }} positions={markers.map(marker => marker.position)} />
                                    <EsriLeafletGeoSearch
                                        position="topleft"
                                        useMapBounds={true}
                                        eventHandlers={{
                                            requeststart: () => console.log("Started request..."),
                                            requestend: () => console.log("Ended request..."),
                                            results: (r) => {
                                                console.log(r)
                                                setCenter([r.latlng.lat, r.latlng.lng])
                                            }
                                        }}
                                        providers={{
                                            arcgisOnlineProvider: {
                                                token: 'AAPKf535a0aedb7b4ff28b1de846f47c5b07QT6RSIkuYAKw-mMxmVPBm6D8wjs2h51eWp_2eRcH-gtD4a_neWO67WECn8K4qdtR',
                                                label: "ArcGIS Online Results",
                                                outFields: ["Place_addr", "PlaceName"]
                                            }
                                        }}
                                    />
                                </MapContainer>
                            </CardBody>
                        </Card>
                    </Col>
                    <Col className='text-center mt-1' xs={12}>
                        <Button type='submit' className='me-1' color='primary' onClick={(e) => {
                            e.preventDefault()
                            handleSumbit(selectedSite)
                        }}>
                            Submit
                        </Button>
                        <Button
                            color='secondary'
                            outline
                            onClick={() => {
                                editShow(!show)
                            }}>
                            Cancel
                        </Button>
                    </Col>
                </Row>
            </ModalBody>
        </Modal>
    )
}

export default SiteFormModal