// ** React Imports
import { Fragment, useEffect, useState } from 'react'
// ** Third Party Components
// ** Styles
import '@styles/base/plugins/maps/map-leaflet.scss'
import 'esri-leaflet-geocoder/dist/esri-leaflet-geocoder.css'

import Site from './site'
import SiteFormModal from './siteFormModal'

// ** Custom Components
import Breadcrumbs from '@components/breadcrumbs'
import illustration from '@src/assets/images/illustration/faq-illustrations.svg'

// ** Reactstrap Imports
import {
    Button,
    Row,
    Col,
    Card,
    CardBody
} from 'reactstrap'

// ** Store & Actions
import { useSelector, useDispatch } from 'react-redux'
import { getSites, addSites, updateSites, deleteSites } from './store'

const Sites = () => {

    // ** States
    const [show, setShow] = useState(false)
    const [selectedSite, setSelectedSite] = useState({
        name: '',
        address: {
            city: '',
            zipCode: 0,
            addressLine: ''
        },
        employees: 0,
        gpsCoordinates: []
    })

    const dispatch = useDispatch()
    const store = useSelector(state => state.sites)
    // ** Hooks

    const onSubmit = data => {
        if (data.id === undefined) {
            dispatch(addSites({ ...data }))
        } else {
            dispatch(updateSites({ ...data }))
        }
        setShow(false)
    }

    const deleteSite = (siteId) => {
        dispatch(deleteSites(siteId))
        return 'success'
    }

    useEffect(() => {
        dispatch(getSites())
    }, [])
    return (
        <Fragment>
            <Breadcrumbs breadCrumbTitle='Sites management' breadCrumbParent='AHR' breadCrumbActive='Sites management' />
            <Row>
                <Col xl={4} md={6}>
                    <Card>
                        <Row>
                            <Col sm={5}>
                                <div className='d-flex align-items-end justify-content-center h-100'>
                                    <img className='img-fluid mt-2' src={illustration} alt='Image' width={85} />
                                </div>
                            </Col>
                            <Col sm={7}>
                                <CardBody className='text-sm-end text-center ps-sm-0'>
                                    <Button
                                        color='primary'
                                        className='text-nowrap mb-1'
                                        onClick={() => {
                                            setSelectedSite({
                                                name: '',
                                                address: {
                                                    city: '',
                                                    zipCode: 0,
                                                    addressLine: ''
                                                },
                                                employees: 0,
                                                gpsCoordinates: []
                                            })
                                            setShow(true)
                                        }}
                                    >
                                        Add new site
                                    </Button>
                                    <p className='mb-0'>Add new site, if it does not exist</p>
                                </CardBody>
                            </Col>
                        </Row>
                    </Card>
                </Col>
            </Row>
            {
                store.sites.length > 0 && (
                    <Row>
                        {store.sites.map(site => <Col md='4' lg='4' key={site.id} ><Site site={site} setSelectedSite={setSelectedSite} editShow={setShow} deleteSite={deleteSite}/></Col>)}
                    </Row>
                )
            }
            <SiteFormModal
                show={show}
                editShow={setShow}
                onSubmit={onSubmit}
                selectedSite={selectedSite} />
        </Fragment >
    )
}
export default Sites