// ** Redux Imports
import { createSlice, createAsyncThunk } from '@reduxjs/toolkit'

// ** Axios Imports
import axios from 'axios'

export const getSites = createAsyncThunk('sites/getSites', async () => {
  const response = await axios.get('/sites/all')
  return response.data
})

export const addSites = createAsyncThunk('sites/addSites', async (site, { dispatch }) => {
  const response = await axios.post('/sites', { site })
  await dispatch(getSites())
  return response.data
})

export const updateSites = createAsyncThunk('sites/updateSites', async (site, { dispatch }) => {
  const response = await axios.put('/sites', { site })
  await dispatch(getSites())
  return response.data
})

export const deleteSites = createAsyncThunk('sites/deleteSites', async (siteId, { dispatch }) => {
  const response = await axios.delete('/sites', { siteId })
  await dispatch(getSites())
  return response.data
})

export const sitesSlice = createSlice({
  name: 'sites',
  initialState: {
    sites: []
  },
  reducers: {
    /* selectEvent: (state, action) => {
      state.selectedEvent = action.payload
    }*/
  },
  extraReducers: builder => {
    builder
      .addCase(getSites.fulfilled, (state, action) => {
        state.sites = action.payload
      })
  }
})

//export const { selectEvent } = appCalendarSlice.actions

export default sitesSlice.reducer