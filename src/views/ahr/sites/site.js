import { Fragment } from "react"
// ** Third Party Components
import Swal from 'sweetalert2'
import withReactContent from 'sweetalert2-react-content'
// ** Reactstrap Imports
import {
    Card,
    CardLink,
    CardBody,
    CardText,
    CardTitle,
    CardSubtitle,
    Button
} from 'reactstrap'

const Site = ({ site, setSelectedSite, editShow, deleteSite }) => {

    const alertDeleteSite = withReactContent(Swal)
    const handleConfirmDeleteSite = (site) => {
        return alertDeleteSite.fire({
            title: 'Are you sure?',
            text: `You want to delete ${site.name} be able to revert this!`,
            icon: 'question',
            showCancelButton: true,
            confirmButtonText: 'Yes, delete it!',
            customClass: {
                confirmButton: 'btn btn-primary',
                cancelButton: 'btn btn-outline-danger ms-1'
            },
            buttonsStyling: false,
            preConfirm: () => {
                return deleteSite(site.id)
            }
        }).then(function (result) {
            if (result.value) {
                alertDeleteSite.fire({
                    icon: 'success',
                    title: 'Deleted!',
                    text: 'Your site has been deleted.',
                    customClass: {
                        confirmButton: 'btn btn-success'
                    }
                })
            }
        })
    }
    return (
        <Fragment>
            <Card className='mb-4'>
                <CardBody className="d-flex justify-content-between flex-sm-row flex-column">
                    <div className='card-information'>
                        <CardTitle tag='h4'>{site.name}</CardTitle>
                        <CardSubtitle className='text-muted mb-1'>{site.address.city}</CardSubtitle>
                        <CardText>
                            {site.address.addressLine}, zip : {site.address.zipCode}
                        </CardText>
                    </div>
                    <div className='d-flex flex-column text-start text-lg-end'>
                        <div className='d-flex order-sm-0 order-1 mt-1 mt-sm-0'>
                            <Button outline color='primary' className='me-75' onClick={() => {
                                setSelectedSite(site)
                                editShow(true)
                            }}>
                                Edit
                            </Button>
                            <Button outline onClick={() => handleConfirmDeleteSite(site)}>Delete</Button>
                        </div>
                        <span className='mt-2'>(+1(968) 945-8832)</span>
                    </div>
                </CardBody>
            </Card>
        </Fragment>
    )
}

export default Site