// ** React Imports
import { Fragment } from 'react'

// ** Reactstrap Imports
import { Card } from 'reactstrap'

// ** Table Import
import NotificationsHistory from './notificationsHistory'

const Notifications = () => {
  return (
    <Fragment>
      <h3>Notifications</h3>
      <p>Use this page to manage Notifications.</p>
      <Card>
        <div className='card-datatable app-user-list table-responsive'>
          <NotificationsHistory />
        </div>
      </Card>
    </Fragment>
  )
}

export default Notifications
