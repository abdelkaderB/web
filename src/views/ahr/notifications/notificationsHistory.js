// ** React Imports
import { Fragment } from 'react'
import { Link } from 'react-router-dom'

// ** Custom Components
import Avatar from '@components/avatar'

// ** Third Party Components
import DataTable from 'react-data-table-component'
import {
  Eye,
  Send,
  Edit,
  Info,
  Copy,
  File,
  Save,
  Trash,
  Printer,
  FileText,
  PieChart,
  Download,
  Clipboard,
  TrendingUp,
  CheckCircle,
  ChevronDown,
  MoreVertical,
  ArrowDownCircle
} from 'react-feather'

// ** Reactstrap Imports
import {
  Button,
  Badge,
  Card,
  CardTitle,
  CardHeader,
  DropdownItem,
  DropdownMenu,
  DropdownToggle,
  UncontrolledTooltip,
  UncontrolledDropdown,
  UncontrolledButtonDropdown,
  UncontrolledPopover, 
  PopoverHeader, 
  PopoverBody } from 'reactstrap'
// ** Styles
import '@styles/react/apps/app-invoice.scss'
import '@styles/react/libs/tables/react-dataTable-component.scss'

// ** Vars
/*const invoiceStatusObj = {
  Sent: { color: 'light-secondary', icon: Send },
  Paid: { color: 'light-success', icon: CheckCircle },
  Draft: { color: 'light-primary', icon: Save },
  Downloaded: { color: 'light-info', icon: ArrowDownCircle },
  'Past Due': { color: 'light-danger', icon: Info },
  'Partial Payment': { color: 'light-warning', icon: PieChart }
}*/

const data = [
  {
    id: 1,
    title: "dev-build issues",
    message: "Note that the development build is not optimized.",
    from: {
        id: 1,
        fullName: "Galen Slixby"
    },
    to: {
        teams: [
          {
            id: 1,
            name: 'Management'
          },
          {
            id: 2,
            name: 'Cleaning Services'
          },
          {
            id: 4,
            name: 'Security and guard services'
          }
        ],
        employee: [
          {
            id: 1,
            name: 'Galen Slixby'
          },
          {
            id: 2,
            name: 'Halsey Redmore'
          },
          {
            id: 4,
            name: 'Cyrill Risby'
          }
        ]
    },
    issuedDate: '14 Oct 2021, 10:20 AM'
  },
  {
    id: 2,
    title: "New bug",
    message: "Note that the client has raised new bug in the checkout process.",
    from: {
        id: 2,
        fullName: "Halsey Redmore"
    },
    to: {
        teams: [],
        employee:[
          {
            id: 3,
            name: 'Marjory Sicely'
          },
          {
            id: 6,
            name: 'Silvain Halstead'
          }
        ]
    },
    issuedDate: '14 Oct 2021, 10:20 AM'
  },
  {
    id: 4,
    title: "Meeting with RH",
    message: "Client satisfaction report is here, meeting in the headquarter office at 9h00 tomorrow.",
    from: {
        id: 2,
        fullName: "Halsey Redmore"
    },
    to: {
        teams: [],
        employee:[
          {
            id: 3,
            name: 'Marjory Sicely'
          },
          {
            id: 6,
            name: 'Silvain Halstead'
          }
        ]
    },
    issuedDate: '14 Oct 2021, 10:20 AM'
  }
]

const columns = [
  {
    name: 'Title',
    sortable: true,
    minWidth: '40px',
    selector: ({ title }) => title,
    cell: row => <span className='text-truncate' >{row.title}</span>
  },
  {
    name: 'Message',
    sortable: true,
    minWidth: '107px',
    selector: ({ message }) => message,
    cell: row => {
      return (
        <Fragment>
          <span className='text-truncate' id={`av-tooltip-${row.id}`}>{row.message}</span>
          <UncontrolledTooltip placement='top' target={`av-tooltip-${row.id}`}>
            <span className='fw-bold'>{row.message}</span>
          </UncontrolledTooltip>
        </Fragment>
      )
    }
  },
  {
    minWidth: '200px',
    name: 'sent to',
    selector: (row) => row.to,
    cell: row => {
      return (
        <Fragment>
          <span>
            Teams(<a href='/' id={`to-tooltip-teams-${row.id}`} onClick={e => e.preventDefault()}><Badge color='light-warning' pill>{row.to.teams.length}</Badge></a>), Employees(<a href='/' id={`to-tooltip-employee-${row.id}`} onClick={e => e.preventDefault()}><Badge color='light-primary' pill>{row.to.employee.length}</Badge></a>)
          </span>
          <UncontrolledPopover trigger='focus' placement='top' target={`to-tooltip-teams-${row.id}`} >
            <PopoverHeader>This notification is sent to</PopoverHeader>
            <PopoverBody>
              <ul>{row.to.teams.map((it, i) => <li key={i}>{it.name} Team</li>)}</ul>
            </PopoverBody>
          </UncontrolledPopover>
          <UncontrolledPopover trigger='focus' placement='top' target={`to-tooltip-employee-${row.id}`} >
            <PopoverHeader>This Notification is sent to</PopoverHeader>
            <PopoverBody>
              <ul>{row.to.employee.map((it, i) => <li key={i}>{it.name}</li>)}</ul>
            </PopoverBody>
          </UncontrolledPopover>
        </Fragment>
      )
    }
  },
  {
    minWidth: '200px',
    name: 'Issued Date',
    cell: row => row.issuedDate,
    selector: ({ issuedDate }) => issuedDate
  },
  {
    name: 'Action',
    minWidth: '110px',
    sortable: true,
    cell: row => (
      <div className='column-action d-flex align-items-center'>
        <Send size={17} id={`send-tooltip-${row.id}`} />
        <UncontrolledTooltip placement='top' target={`send-tooltip-${row.id}`}>
          Send Notification
        </UncontrolledTooltip>
        <Link to={`/apps/invoice/preview/${row.id}`} id={`pw-tooltip-${row.id}`} onClick={e => e.preventDefault()}>
          <Eye size={17} className='mx-1' />
        </Link>
        <UncontrolledTooltip placement='top' target={`pw-tooltip-${row.id}`}>
          Preview Invoice
        </UncontrolledTooltip>
        <UncontrolledDropdown>
          <DropdownToggle tag='span'>
            <MoreVertical size={17} className='cursor-pointer' />
          </DropdownToggle>
          <DropdownMenu end>
            <DropdownItem tag='a' href='/' className='w-100' onClick={e => e.preventDefault()}>
              <Download size={14} className='me-50' />
              <span className='align-middle'>Download</span>
            </DropdownItem>
            <DropdownItem tag={Link} to={`/apps/invoice/edit/${row.id}`} className='w-100'  onClick={e => e.preventDefault()}>
              <Edit size={14} className='me-50' />
              <span className='align-middle'>Edit</span>
            </DropdownItem>
            <DropdownItem tag='a' href='/' className='w-100' onClick={e => e.preventDefault()}>
              <Trash size={14} className='me-50' />
              <span className='align-middle'>Delete</span>
            </DropdownItem>
            <DropdownItem tag='a' href='/' className='w-100' onClick={e => e.preventDefault()}>
              <Copy size={14} className='me-50' />
              <span className='align-middle'>Duplicate</span>
            </DropdownItem>
          </DropdownMenu>
        </UncontrolledDropdown>
      </div>
    )
  }
]

const NotificationsHistory = () => (
  <div className='invoice-list-wrapper'>
    <Card>
      <CardHeader className='py-1'>
        <CardTitle tag='h4'>notifications</CardTitle>
        <div className='d-flex'>
          <Button className='add-permission me-1' color='primary'>
              Add new team
          </Button>
          <UncontrolledButtonDropdown>
            <DropdownToggle outline caret>
              Export
            </DropdownToggle>
            <DropdownMenu end>
              <DropdownItem className='w-100'>
                <Printer className='font-small-4 me-50' />
                <span>Print</span>
              </DropdownItem>
              <DropdownItem className='w-100'>
                <FileText className='font-small-4 me-50' />
                <span>CSV</span>
              </DropdownItem>
              <DropdownItem className='w-100'>
                <File className='font-small-4 me-50' />
                <span>Excel</span>
              </DropdownItem>
              <DropdownItem className='w-100'>
                <Clipboard className='font-small-4 me-50' />
                <span>PDF</span>
              </DropdownItem>
              <DropdownItem className='w-100'>
                <Copy className='font-small-4 me-50' />
                <span>Copy</span>
              </DropdownItem>
            </DropdownMenu>
          </UncontrolledButtonDropdown>
          
        </div>
        
      </CardHeader>
      <div className='invoice-list-dataTable react-dataTable'>
        <DataTable
          noHeader
          responsive
          data={data}
          columns={columns}
          className='react-dataTable'
          sortIcon={<ChevronDown size={10} />}
        />
      </div>
    </Card>
  </div>
)

export default NotificationsHistory
