// ** React Imports
import { Link } from 'react-router-dom'

// ** Third Party Components
import classnames from 'classnames'

// ** Reactstrap Imports
import { Badge } from 'reactstrap'

// ** Vars
/*const colors = {
  support: 'light-info',
  user: 'light-success',
  manager: 'light-warning',
  administrator: 'light-primary',
  'restricted-user': 'light-danger'
}*/

export const columns = [
  {
    name: 'Name',
    sortable: true,
    minWidth: '300px',
    cell: ({ name }) => name,
    selector: row => row.name
  },
  {
    sortable: true,
    minWidth: '250px',
    name: 'Supervisor',
    selector: row => row.supervisor,
    cell: ({ supervisor }) => {
      if (supervisor !== undefined) {
        return (           
          <Link key={`${supervisor.id}`} to={`/ahr/employees/view/${supervisor.id}`} className={classnames('me-50')}>
              {supervisor.name}
          </Link>)
      }
    }
  },
  {
    sortable: true,
    minWidth: '200px',
    name: 'Team employee count',
    selector: row => row.employeeCount,
    cell: ({ employeeCount }) => employeeCount
  },
  {
    sortable: true,
    minWidth: '350px',
    name: 'Created Date',
    selector: row => row.createdDate,
    cell: ({ createdDate }) => createdDate,
    sortFunction: (rowA, rowB) => {
      return new Date(rowB.createdDate) - new Date(rowA.createdDate)
    }
  }
]
