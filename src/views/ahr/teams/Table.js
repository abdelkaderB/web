// ** React Imports
import { useEffect, useState, Fragment } from 'react'

// ** Table Columns
import { columns } from './columns'
import Select, { components }  from 'react-select'

// ** Utils
import { selectThemeColors } from '@utils'

// ** Reactstrap Imports
import { Alert, Row, Col, Label, Form, Input, Button, Modal, ModalHeader, ModalBody, FormFeedback } from 'reactstrap'

// ** Store & Actions
import { useDispatch, useSelector } from 'react-redux'
import { getData, addTeam, deleteTeam, selectTeam, updateTeam } from './store'
import { getAllData as getEmployees } from '../employees/store'

// ** Third party Components
import classnames from 'classnames'
import ReactPaginate from 'react-paginate'
import DataTable from 'react-data-table-component'
import { useForm, Controller } from 'react-hook-form'
import { ChevronDown, Edit, Trash } from 'react-feather'

// ** Styles
import '@styles/react/libs/tables/react-dataTable-component.scss'

const CustomHeader = ({
  setShow,
  searchTerm,
  rowsPerPage,
  handlePerPage,
  handleFilter}) => {
  return (
    <Row className='text-nowrap w-100 my-75 g-0 permission-header'>
      <Col xs={12} lg={4} className='d-flex align-items-center'>
        <div className='d-flex align-items-center justify-content-center justify-content-lg-start'>
          <label htmlFor='rows-per-page'>Show</label>
          <Input
            className='mx-50'
            type='select'
            id='rows-per-page'
            value={rowsPerPage}
            onChange={handlePerPage}
            style={{ width: '5rem' }}
          >
            <option value='10'>10</option>
            <option value='25'>25</option>
            <option value='50'>50</option>
          </Input>
          <label htmlFor='rows-per-page'>Entries</label>
        </div>
      </Col>
      <Col xs={12} lg={8}>
        <div className='d-flex align-items-center justify-content-lg-end justify-content-start flex-md-nowrap flex-wrap mt-lg-0 mt-1'>
          <div className='d-flex align-items-center me-1'>
            <label className='mb-0' htmlFor='search-team'>
              Search:
            </label>
            <Input
              type='text'
              value={searchTerm}
              id='search-team'
              className='ms-50 w-100'
              onChange={e => handleFilter(e.target.value)}
            />
          </div>
          <Button className='add-permission mt-sm-0 mt-1' color='primary' onClick={() => setShow(true)}>
            Add new team
          </Button>
        </div>
      </Col>
    </Row>
  )
}

const Table = () => {
  // ** Store Vars & Hooks
  const dispatch = useDispatch()
  const store = useSelector(state => ({teams: state.teams, employees: state.employees, selected: state.teams.selected}))
  const {
    reset,
    control,
    setError,
    setValue,
    handleSubmit,
    formState: { errors }
  } = useForm({ defaultValues: { teamName: '' } })

  // ** States
  const [show, setShow] = useState(false)
  const [assignedTo, setAssignedTo] = useState('')
  const [searchTerm, setSearchTerm] = useState('')
  const [currentPage, setCurrentPage] = useState(1)
  const [rowsPerPage, setRowsPerPage] = useState(10)
  const [supervisor, setSupervisor] = useState('')
  // ** Get data on mount
  useEffect(() => {
    dispatch(getEmployees())
    dispatch(
      getData({
        assignedTo,
        q: searchTerm,
        page: currentPage,
        perPage: rowsPerPage
      })
    )
  }, [dispatch, store.teams.data.lenght])

  // ** Function in get data on page change
  const handlePagination = page => {
    dispatch(
      getData({
        assignedTo,
        q: searchTerm,
        perPage: rowsPerPage,
        page: page.selected + 1
      })
    )
    setCurrentPage(page.selected + 1)
  }

  // ** Function in get data on rows per page
  const handlePerPage = e => {
    const value = parseInt(e.currentTarget.value)
    dispatch(
      getData({
        assignedTo,
        q: searchTerm,
        perPage: value,
        page: currentPage
      })
    )
    setRowsPerPage(value)
  }

  // ** Function in get data on search query change
  const handleFilter = val => {
    setSearchTerm(val)
    dispatch(
      getData({
        q: val,
        assignedTo,
        page: currentPage,
        perPage: rowsPerPage
      })
    )
  }

  // ** Function to filter Roles
  const handleAssignedToChange = val => {
    setAssignedTo(val)
    dispatch(
      getData({
        q: searchTerm,
        assignedTo: val,
        page: currentPage,
        perPage: rowsPerPage
      })
    )
  }

  // ** Custom Pagination
  const CustomPagination = () => {
    const count = Number(Math.ceil(store.teams.total / rowsPerPage))

    return (
      <ReactPaginate
        previousLabel={''}
        nextLabel={''}
        pageCount={count || 1}
        activeClassName='active'
        forcePage={currentPage !== 0 ? currentPage - 1 : 0}
        onPageChange={page => handlePagination(page)}
        pageClassName={'page-item'}
        nextLinkClassName={'page-link'}
        nextClassName={'page-item next'}
        previousClassName={'page-item prev'}
        previousLinkClassName={'page-link'}
        pageLinkClassName={'page-link'}
        containerClassName={'pagination react-paginate justify-content-end my-2 pe-1'}
      />
    )
  }

  // ** Table data to render
  const dataToRender = () => {
    const filters = {
      q: searchTerm
    }

    const isFiltered = Object.keys(filters).some(function (k) {
      return filters[k].length > 0
    })

    if (store.teams.data.length > 0) {
      return store.teams.data
    } else if (store.teams.data.length === 0 && isFiltered) {
      return []
    } else {
      return store.teams.allData.slice(0, rowsPerPage)
    }
  }

  const handleEditClick = data => {
    dispatch(selectTeam(data))
    setValue('teamName', data.name)
    setShow(true)
  }

  const handleModalClosed = () => {
    dispatch(selectTeam(null))
    setValue('teamName', '')
  }

  const onSubmit = data => {
    if (data.teamName.length) {
      if (store.teams.selected !== null) {
        dispatch(updateTeam({ id: store.teams.selected.id, name: data.teamName, supervisor: {id: supervisor.value, name: supervisor.label} }))
      } else {
        dispatch(addTeam({ name: data.teamName, supervisor: {id: supervisor.value, name: supervisor.label} }))
      }
      setShow(false)
    } else {
      setError('teamName', {
        type: 'manual'
      })
    }
  }

  const updatedColumns = [
    ...columns,
    {
      name: 'Actions',
      cell: row => {
        return (
          <div className='d-flex align-items-center permissions-actions'>
            <Button size='sm' color='transparent' className='btn btn-icon' onClick={() => handleEditClick(row)}>
              <Edit className='font-medium-2' />
            </Button>
            <Button
              size='sm'
              color='transparent'
              className='btn btn-icon'
              onClick={() => dispatch(deleteTeam(row.id))}
            >
              <Trash className='font-medium-2' />
            </Button>
          </div>
        )
      }
    }
  ]

  const handleDiscard = () => {
    reset()
    setShow(false)
  }

  // ** Custom Assignee Component
  const AssigneeComponent = ({ data, ...props }) => {
    return (
      <components.Option {...props}>
        <div className='d-flex align-items-center'>
          <p className='mb-0'>{data.label}</p>
        </div>
      </components.Option>
    )
  }

  const renderForm = () => {
    if (store.teams.selected === null) {
      return (
        <Row tag={Form} onSubmit={handleSubmit(onSubmit)}>
          <Col xs={12}>
            <Label className='form-label' for='permission-name'>
              Team Name
            </Label>
            <Controller
              control={control}
              id='teamName'
              name='teamName'
              render={({ field }) => (
                <Input placeholder='Team Name' invalid={errors.teamName && true} {...field} />
              )}
            />
            {errors && errors.teamName && <FormFeedback>Please enter a valid Team Name</FormFeedback>}
          </Col>
          <Col xs={12} style={{marginTop: 20, marginBottom: 20}}>
            <Label className='form-label' for='team-supervisor'>
              Supervisor
            </Label>
            <Select
              id='team-supervisor'
              className='react-select'
              classNamePrefix='select'
              isClearable={false}
              options={store.employees.allData.map(it => ({value: it.id, label: it.fullName}))}
              theme={selectThemeColors}
              value={supervisor}
              onChange={data => setSupervisor(data)}
              components={{ Option: AssigneeComponent }}
            />
          </Col>
          <Col xs={12} className='text-center mt-2'>
            <Button className='me-1' color='primary'>
              Create Team
            </Button>
            <Button outline type='reset' onClick={handleDiscard}>
              Discard
            </Button>
          </Col>
        </Row>
      )
    } else {
      return (
        <Fragment>
          <Row tag={Form} onSubmit={handleSubmit(onSubmit)}>
            <Col xs={12} sm={9}>
              <Label className='form-label' for='team-name'>
                Team Name
              </Label>
              <Controller
                control={control}
                id='teamName'
                name='teamName'
                render={({ field }) => (
                  <Input placeholder='Team Name' invalid={errors.teamName && true} {...field} />
                )}
              />
              {errors && errors.teamName && <FormFeedback>Please enter a valid Team Name</FormFeedback>}
            </Col>
            <Col xs={12} sm={9} style={{marginTop: 20, marginBottom: 20}}>
              <Label className='form-label' for='team-supervisor'>
                Supervisor
              </Label>
              <Select
                id='team-supervisor'
                className='react-select'
                classNamePrefix='select'
                isClearable={false}
                options={store.employees.allData.map(it => ({value: it.id, label: it.fullName}))}
                theme={selectThemeColors}
                value={supervisor}
                onChange={data => setSupervisor(data)}
                components={{ Option: AssigneeComponent }}
              />
              <br/>
              <Button className='mt-2' color='primary'>
                Update
              </Button>
            </Col>
          </Row>
        </Fragment>
      )
    }
  }

  return (
    <Fragment>
      <div className='react-dataTable'>
        <DataTable
          noHeader
          pagination
          subHeader
          responsive
          paginationServer
          columns={updatedColumns}
          sortIcon={<ChevronDown />}
          className='react-dataTable'
          paginationComponent={CustomPagination}
          data={dataToRender()}
          subHeaderComponent={
            <CustomHeader
              setShow={setShow}
              assignedTo={assignedTo}
              searchTerm={searchTerm}
              rowsPerPage={rowsPerPage}
              handleFilter={handleFilter}
              handlePerPage={handlePerPage}
              handleAssignedToChange={handleAssignedToChange}
            />
          }
        />
      </div>
      <Modal isOpen={show} onClosed={handleModalClosed} toggle={() => setShow(!show)} className='modal-dialog-centered'>
        <ModalHeader className='bg-transparent' toggle={() => setShow(!show)}></ModalHeader>
        <ModalBody
          className={classnames({
            'p-3 pt-0': store.teams.selected !== null,
            'px-sm-5 pb-5': store.teams.selected === null
          })}
        >
          <div className='text-center mb-2'>
            <h1 className='mb-1'>{store.teams.selected !== null ? 'Edit' : 'Add New'} Team</h1>
          </div>

          {renderForm()}
        </ModalBody>
      </Modal>
    </Fragment>
  )
}

export default Table
