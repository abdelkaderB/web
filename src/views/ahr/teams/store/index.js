// ** Redux Imports
import { createSlice, createAsyncThunk } from '@reduxjs/toolkit'

// ** Axios Imports
import axios from 'axios'

export const getData = createAsyncThunk('teams/getData', async params => {
  const response = await axios.get('/teams/data', { params })
  return {
    total: response.data.total,
    params: response.data.params,
    allData: response.data.allData,
    data: response.data.teams
  }
})

export const getAllData = createAsyncThunk('teams/getAllData', async () => {
  const response = await axios.get('/teams/all-data')
  return response.data
})

export const addTeam = createAsyncThunk(
  'teams/addTeam',
  async (team, { dispatch, getState }) => {
    await axios.post('/teams/add-team', { team })
    await dispatch(getData(getState().teams.params))
    return team
  }
)

export const updateTeam = createAsyncThunk(
  'teams/updateTeam',
  async ({ id, name, supervisor }, { dispatch, getState }) => {
    await axios.post('/teams/update-team', { id, name, supervisor })
    await dispatch(getData(getState().teams.params))
    return { id, name, supervisor }
  }
)

export const deleteTeam = createAsyncThunk('teams/deleteTeams', async (id, { dispatch, getState }) => {
  await axios.delete('/teams/delete', { id })
  await dispatch(getData(getState().teams.params))
  return id
})

export const teamsSlice = createSlice({
  name: 'teams',
  initialState: {
    data: [],
    total: 1,
    params: {},
    allData: [],
    selected: null
  },
  reducers: {
    selectTeam: (state, action) => {
      if (action.payload === null) {
        state.selected = null
      } else {
        state.selected = action.payload
      }
    }
  },
  extraReducers: builder => {
    builder
    .addCase(getAllData.fulfilled, (state, action) => {
      state.allData = action.payload
    }).addCase(getData.fulfilled, (state, action) => {
      state.data = action.payload.data
      state.total = action.payload.total
      state.params = action.payload.params
      state.allData = action.payload.allData
    })
  }
})

export const { selectTeam } = teamsSlice.actions

export default teamsSlice.reducer
